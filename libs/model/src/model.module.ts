import { Module } from '@nestjs/common';
import { Customer } from './Customer.model';
import { PointBag } from './PointBag.model';
import { Concept } from './Concept.model';
import { PointUseDetail } from './PointUseDetail.model';
import { PointUseHeader } from './PointUseHeader.model';
import { PointAllocationRule } from './PointAllocationRule.model';

@Module({
  providers: [Concept, Customer, PointAllocationRule, PointBag, PointUseDetail, PointUseHeader],
  exports: [Concept, Customer, PointAllocationRule, PointBag, PointUseDetail, PointUseHeader],
})
export class ModelModule { }
