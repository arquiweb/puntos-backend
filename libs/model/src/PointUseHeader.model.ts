export class PointUseHeader {
  id: number;
  customerId: number;
  puntajeUtilizado: number;
  fecha: string;
  conceptoId: number;
}
