export class PointBag {
  id: number;
  customerId: number;
  fechaAsignacion: Date;
  fechaCaducidad: Date;
  puntajeAsignado: number;
  puntajeUtilizado: number;
  saldoPuntos: number;
  montoOperacion: number;
}
