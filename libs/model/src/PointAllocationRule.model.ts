export class PointAllocationRule {
  id: number;
  limiteInferior: number;
  limiteSuperior: number;
  equivalencia: number; // monto de equivalencia de 1 punto
}
