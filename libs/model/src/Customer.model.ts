export class Customer {
  id: number;
  nombre: string;
  apellido: string;
  numeroDocumento: string;
  tipoDocumento: string;
  nacionalidad: string;
  email: string;
  telefono: string;
  fechaNacimiento: string;
}
