import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class PointExpiration {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  fechaInicioValidez: string;

  @Column()
  fechaFinValidez: string;

  @Column()
  diasDuracion: number;
}
