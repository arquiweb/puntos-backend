import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PointExpiration } from './point-expiration.entity';
import { PointExpirationService } from './point-expiration.service';
import { PointExpirationController } from './point-expiration.controller';
import { PointBagModule } from 'src/point-bag/point-bag.module';

@Module({
  imports: [TypeOrmModule.forFeature([PointExpiration]), forwardRef(() => PointBagModule)],
  controllers: [PointExpirationController],
  providers: [PointExpirationService],
  exports: [PointExpirationService],
})
export class PointExpirationModule {}
