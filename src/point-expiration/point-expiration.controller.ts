import { Controller, Get, Post, Body, Put, Param, Delete } from '@nestjs/common';
import { PointExpirationService } from './point-expiration.service';
import { CreatePointExpirationDto } from './create-point-expiration.dto';

@Controller('point-expiration')
export class PointExpirationController {
  constructor(private readonly pointExpirationService: PointExpirationService) {}

  @Post()
  create(@Body() createPointExpirationDto: CreatePointExpirationDto) {
    return this.pointExpirationService.create(createPointExpirationDto);
  }

  @Get()
  findAll() {
    return this.pointExpirationService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: number) {
    return this.pointExpirationService.findOne(id);
  }

  @Put(':id')
  update(@Param('id') id: number, @Body() updatePointExpirationDto: CreatePointExpirationDto) {
    return this.pointExpirationService.update(id, updatePointExpirationDto);
  }

  @Delete(':id')
  remove(@Param('id') id: number) {
    return this.pointExpirationService.remove(id);
  }
}
