import { IsString, IsNotEmpty, IsInt, Min } from 'class-validator';

export class CreatePointExpirationDto {
  @IsString()
  @IsNotEmpty()
  fechaInicioValidez: string;

  @IsString()
  @IsNotEmpty()
  fechaFinValidez: string;

  @IsInt()
  @Min(1)
  diasDuracion: number;
}
