import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PointExpiration } from './point-expiration.entity';
import { CreatePointExpirationDto } from './create-point-expiration.dto';
import { Cron, CronExpression } from '@nestjs/schedule';
import { PointBagService } from 'src/point-bag/point-bag.service';

@Injectable()
export class PointExpirationService {
  constructor(
    @Inject(forwardRef(() => PointBagService))
    private readonly pointBagService: PointBagService,
    @InjectRepository(PointExpiration)
    private pointExpirationRepository: Repository<PointExpiration>
  ) {}

  create(createPointExpirationDto: CreatePointExpirationDto): Promise<PointExpiration> {
    const pointExpiration = this.pointExpirationRepository.create(createPointExpirationDto);
    return this.pointExpirationRepository.save(pointExpiration);
  }

  findAll(): Promise<PointExpiration[]> {
    return this.pointExpirationRepository.find();
  }

  findOne(id: number): Promise<PointExpiration> {
    return this.pointExpirationRepository.findOneBy({ id });
  }

  async update(id: number, updatePointExpirationDto: CreatePointExpirationDto): Promise<PointExpiration> {
    await this.pointExpirationRepository.update(id, updatePointExpirationDto);
    return this.findOne(id);
  }

  async remove(id: number): Promise<void> {
    await this.pointExpirationRepository.delete(id);
  }

  async getDiasDuracion(): Promise<number> {
    const expirationParam = await this.pointExpirationRepository.findOne({
      where: {},  // Se asegura de que siempre haya condiciones, aunque vacías
      order: {
        fechaFinValidez: 'DESC'
      }
    });
    return expirationParam ? expirationParam.diasDuracion : 0;
  }

  @Cron(CronExpression.EVERY_HOUR)
  async handleCron() {
    console.log('Running the scheduled task to update expired points.');
    const now = new Date();
    let expiredBags = await this.pointBagService.findAll()
    expiredBags = expiredBags.filter(bag => bag.fechaCaducidad < now && bag.saldoPuntos > 0);

    expiredBags.forEach(bag => {
      bag.puntajeUtilizado += bag.saldoPuntos;
      bag.saldoPuntos = 0;
    });

    this.pointBagService.updateBags(expiredBags);
  }

}
