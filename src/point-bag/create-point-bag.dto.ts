// create-point-bag.dto.ts
import { IsDateString, IsInt, IsNotEmpty, IsNumber, IsPositive } from 'class-validator';

export class CreatePointBagDto {
  @IsInt()
  @IsNotEmpty()
  idCliente: number;

  @IsDateString()
  @IsNotEmpty()
  fechaAsignacion: Date;

  @IsNumber()
  @IsPositive()
  montoOperacion: number;

  @IsInt()
  @IsNotEmpty()
  puntajeAsignado: number;

  @IsInt()
  puntajeUtilizado: number;

  @IsInt()
  saldoPuntos: number;
}
