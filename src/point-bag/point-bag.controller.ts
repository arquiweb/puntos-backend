import { Controller, Get, Post, Body, Param, Delete, Put } from '@nestjs/common';
import { PointBagService } from './point-bag.service';
import { CreatePointBagDto } from './create-point-bag.dto';

@Controller('point-bag')
export class PointBagController {
  constructor(private readonly pointBagService: PointBagService) {}

  @Post()
  create(@Body() createPointBagDto: CreatePointBagDto) {
    return this.pointBagService.create(createPointBagDto);
  }

  @Get()
  findAll() {
    return this.pointBagService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: number) {
    return this.pointBagService.findOne(id);
  }

  @Put(':id')
  update(@Param('id') id: number, @Body() createPointBagDto: CreatePointBagDto) {
    return this.pointBagService.update(id, createPointBagDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.pointBagService.remove(id);
  }
}
