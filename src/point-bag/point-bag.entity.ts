import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';
import { Customer } from '../customer/customer.entity';

@Entity()
export class PointBag {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Customer)
  @JoinColumn({ name: 'idCliente' })
  customer: Customer;

  @Column({ type: 'date' })
  fechaAsignacion: Date;

  @Column({ type: 'date' })
  fechaCaducidad: Date;

  @Column()
  puntajeAsignado: number;

  @Column({ nullable: true, default: 0 })
  puntajeUtilizado: number | null;

  @Column()
  saldoPuntos: number;

  @Column()
  montoOperacion: number;

  @Column()
  idCliente: number;
}
