import { forwardRef, Inject, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PointBag } from './point-bag.entity';
import { CreatePointBagDto } from './create-point-bag.dto';
import { PointExpirationService } from '../point-expiration/point-expiration.service';
import * as dayjs from 'dayjs';

@Injectable()
export class PointBagService {
  constructor(
    @InjectRepository(PointBag)
    private readonly pointBagRepository: Repository<PointBag>,
    @Inject(forwardRef(() => PointExpirationService))
    private readonly pointExpirationService: PointExpirationService,
  ) {}

  async create(createPointBagDto: CreatePointBagDto): Promise<PointBag> {
    const diasDuracion = await this.pointExpirationService.getDiasDuracion();
    const fechaCaducidad = dayjs(createPointBagDto.fechaAsignacion).add(diasDuracion, 'day').toDate();

    const pointBag = this.pointBagRepository.create({
      ...createPointBagDto,
      fechaCaducidad,
    });

    return this.pointBagRepository.save(pointBag);
  }

  async findAll(): Promise<PointBag[]> {
    return this.pointBagRepository.find();
  }

  async findOne(id: number): Promise<PointBag> {
    return this.pointBagRepository.findOne({ where: { id } });
  }

  async update(id: number, createPointBagDto: CreatePointBagDto): Promise<PointBag> {
    const diasDuracion = await this.pointExpirationService.getDiasDuracion();
    const fechaCaducidad = dayjs(createPointBagDto.fechaAsignacion).add(diasDuracion, 'day').toDate();

    await this.pointBagRepository.update(id, {
      ...createPointBagDto,
      fechaCaducidad,
    });

    return this.findOne(id);
  }

  async remove(id: string): Promise<void> {
    const result = await this.pointBagRepository.delete(id);
    if (result.affected === 0) {
      throw new NotFoundException('Bolsa de puntos no encontrada');
    }
  }

  updateBags(bags: PointBag[]): void {
    bags.forEach(bag => {
      this.pointBagRepository.update(bag.id, bag);
    });
  }
}
