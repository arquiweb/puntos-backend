import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PointBagService } from './point-bag.service';
import { PointBagController } from './point-bag.controller';
import { PointBag } from './point-bag.entity';
import { PointExpirationModule } from '../point-expiration/point-expiration.module'; 
import { CustomerModule } from '../customer/customer.module'; 
import { ReglaAsignacionPuntoModule } from '../point-ruler/regla-asignacion-punto.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([PointBag]),
    forwardRef(() => PointExpirationModule), 
    CustomerModule, 
    ReglaAsignacionPuntoModule,
  ],
  controllers: [PointBagController],
  providers: [PointBagService],
  exports: [PointBagService],
})
export class PointBagModule {}
