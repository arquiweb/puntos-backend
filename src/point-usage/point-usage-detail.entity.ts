import { Entity, Column, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { PointUsage } from './point-usage.entity';
import { PointBag } from '../point-bag/point-bag.entity';

@Entity()
export class PointUsageDetail {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => PointUsage, pointUsage => pointUsage.detalles, { onDelete: 'CASCADE' })
  pointUsage: PointUsage;

  @ManyToOne(() => PointBag, { eager: true })
  pointBag: PointBag;

  @Column()
  puntajeUtilizado: number;
}
