// point-usage.controller.ts
import { Controller, Get, Post, Body, Param, Delete } from '@nestjs/common';
import { PointUsageService } from './point-usage.service';
import { CreatePointUsageDto } from './create-point-usage.dto';

@Controller('point-usage')
export class PointUsageController {
  constructor(private readonly pointUsageService: PointUsageService) {}

  @Post()
  create(@Body() createPointUsageDto: CreatePointUsageDto) {
    return this.pointUsageService.create(createPointUsageDto);
  }

  @Get()
  findAll() {
    return this.pointUsageService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: number) {
    return this.pointUsageService.findOne(id);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.pointUsageService.remove(id);
  }
}
