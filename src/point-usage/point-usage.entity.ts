import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany } from 'typeorm';
import { Customer } from '../customer/customer.entity';
import { PointUsageDetail } from './point-usage-detail.entity';

@Entity()
export class PointUsage {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Customer)
  customer: Customer;

  @Column()
  puntajeUtilizado: number;

  @Column({ type: 'date' })
  fecha: Date;

  @Column()
  conceptoUsoPunto: string;

  @OneToMany(() => PointUsageDetail, (detail: PointUsageDetail) => detail.pointUsage)
  detalles: PointUsageDetail[];
}
