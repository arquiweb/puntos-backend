// create-point-usage.dto.ts
import { IsDateString, IsInt, IsNotEmpty, IsString } from 'class-validator';
import { Type } from 'class-transformer';
import { CreatePointUsageDetailDto } from './create-point-usage-detail.dto';

export class CreatePointUsageDto {
  @IsInt()
  @IsNotEmpty()
  idCliente: number;

  @IsInt()
  @IsNotEmpty()
  puntajeUtilizado: number;

  @IsDateString()
  @IsNotEmpty()
  fecha: Date;

  @IsString()
  @IsNotEmpty()
  conceptoUsoPunto: string;

  @Type(() => CreatePointUsageDetailDto)
  detalles: CreatePointUsageDetailDto[];
}
