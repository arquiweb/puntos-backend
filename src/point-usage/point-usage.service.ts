// point-usage.service.ts
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PointUsage } from './point-usage.entity';
import { CreatePointUsageDto } from './create-point-usage.dto';
import { PointBag } from '../point-bag/point-bag.entity';
import { PointUsageDetail } from './point-usage-detail.entity';
import { Customer } from 'src/customer/customer.entity';
import { ConceptoUsoPunto } from '../point-concept/concepto-uso-punto.entity';
import { EntityManager } from 'typeorm';

@Injectable()
export class PointUsageService {
  constructor(
    @InjectRepository(PointUsage)
    private readonly pointUsageRepository: Repository<PointUsage>,
    @InjectRepository(PointBag)
    private readonly pointBagRepository: Repository<PointBag>,
    @InjectRepository(PointUsageDetail)
    private readonly pointUsageDetailRepository: Repository<PointUsageDetail>,
    @InjectRepository(ConceptoUsoPunto)
    private readonly conceptoUsoPuntoRepository: Repository<ConceptoUsoPunto>,
    private readonly entityManager: EntityManager,
  ) {}

  async create(createPointUsageDto: CreatePointUsageDto): Promise<PointUsage> {
    return this.entityManager.transaction(async transactionalEntityManager => {
      const { idCliente, conceptoUsoPunto, fecha } = createPointUsageDto;
  
      console.log('Received DTO:', createPointUsageDto);  // Log received DTO
  
      const concepto = await transactionalEntityManager.findOne(ConceptoUsoPunto, {
        where: { id: Number(conceptoUsoPunto) },
      });
  
      if (!concepto) {
        throw new Error('Concepto de uso de puntos no encontrado');
      }
  
      const puntajeUtilizado = concepto.puntosRequeridos;
  
      const pointBags = await transactionalEntityManager.find(PointBag, {
        where: { idCliente },
        order: { fechaAsignacion: 'ASC' },
      });
  
      let remainingPoints = puntajeUtilizado;
      const pointUsageDetails = [];
  
      for (const pointBag of pointBags) {
        if (remainingPoints <= 0) break;
        const pointsToDeduct = Math.min(remainingPoints, pointBag.saldoPuntos);
  
        pointBag.saldoPuntos -= pointsToDeduct;
        remainingPoints -= pointsToDeduct;
  
        const detail = transactionalEntityManager.create(PointUsageDetail, {
          pointBag,
          puntajeUtilizado: pointsToDeduct,
        });
  
        pointUsageDetails.push(detail);
        await transactionalEntityManager.save(pointBag);
      }
  
      const customer = await transactionalEntityManager.findOne(Customer, { where: { id: idCliente } });
      if (!customer) {
        throw new Error('Cliente no encontrado');
      }
  
      const pointUsage = transactionalEntityManager.create(PointUsage, {
        customer,
        puntajeUtilizado,
        fecha,
        conceptoUsoPunto: concepto.descripcion,
        detalles: pointUsageDetails,
      });
  
      console.log('PointUsage to be saved:', pointUsage);  // Log point usage to be saved
  
      await transactionalEntityManager.save(pointUsage);
      await transactionalEntityManager.save(pointUsageDetails);
  
      return pointUsage;
    });
  }
  
  async findAll(): Promise<PointUsage[]> {
    return this.pointUsageRepository.find({ relations: ['detalles', 'detalles.pointBag'] });
  }

  async findOne(id: number): Promise<PointUsage> {
    return this.pointUsageRepository.findOne({ where: { id } });
  }

  async remove(id: string): Promise<void> {
    await this.pointUsageRepository.delete(id);
  }
}
