import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PointUsageService } from './point-usage.service';
import { PointUsageController } from './point-usage.controller';
import { PointUsage } from './point-usage.entity';
import { PointBag } from '../point-bag/point-bag.entity';
import { PointUsageDetail } from './point-usage-detail.entity';
import { ConceptoUsoPunto } from '../point-concept/concepto-uso-punto.entity'; 
import { ConceptoUsoPuntoModule } from '../point-concept/concepto-uso-punto.module'; 
import { Customer } from '../customer/customer.entity'; // Importar la entidad Customer

@Module({
  imports: [
    TypeOrmModule.forFeature([PointUsage, PointBag, PointUsageDetail, ConceptoUsoPunto, Customer]), // Añadir Customer aquí
    ConceptoUsoPuntoModule,
  ],
  providers: [PointUsageService],
  controllers: [PointUsageController],
})
export class PointUsageModule {}
