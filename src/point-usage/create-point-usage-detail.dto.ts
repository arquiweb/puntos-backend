import { IsInt, IsNotEmpty } from 'class-validator';

export class CreatePointUsageDetailDto {
  @IsInt()
  @IsNotEmpty()
  idBolsa: number;

  @IsInt()
  @IsNotEmpty()
  puntajeUtilizado: number;
}
