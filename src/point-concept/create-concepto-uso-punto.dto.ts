import { IsString, IsNotEmpty, IsInt, Min } from 'class-validator';

export class CreateConceptoUsoPuntoDto {
    readonly puntosRequeridos: number;
    readonly descripcion: string;
  }