import { Controller, Get, Post, Body, Put, Param, Delete } from '@nestjs/common';
import { ConceptoUsoPuntoService } from './concepto-uso-punto.service';
import { CreateConceptoUsoPuntoDto } from './create-concepto-uso-punto.dto';

@Controller('concepto-uso-punto')
export class ConceptoUsoPuntoController {
  constructor(private readonly conceptoUsoPuntoService: ConceptoUsoPuntoService) {}

  @Post()
  create(@Body() createConceptoUsoPuntoDto: CreateConceptoUsoPuntoDto) {
    return this.conceptoUsoPuntoService.create(createConceptoUsoPuntoDto);
  }

  @Get()
  findAll() {
    return this.conceptoUsoPuntoService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: number) {
    return this.conceptoUsoPuntoService.findOne(id);
  }

  @Put(':id')
  update(@Param('id') id: number, @Body() updateConceptoUsoPuntoDto: CreateConceptoUsoPuntoDto) {
    return this.conceptoUsoPuntoService.update(id, updateConceptoUsoPuntoDto);
  }

  @Delete(':id')
  remove(@Param('id') id: number) {
    return this.conceptoUsoPuntoService.remove(id);
  }
}
