import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConceptoUsoPunto } from './concepto-uso-punto.entity';
import { ConceptoUsoPuntoService } from './concepto-uso-punto.service';
import { ConceptoUsoPuntoController } from './concepto-uso-punto.controller';

@Module({
  imports: [TypeOrmModule.forFeature([ConceptoUsoPunto])],
  providers: [ConceptoUsoPuntoService],
  controllers: [ConceptoUsoPuntoController],
})
export class ConceptoUsoPuntoModule {}
