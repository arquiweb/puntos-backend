import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class ConceptoUsoPunto {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  descripcion: string;

  @Column()
  puntosRequeridos: number;
}
