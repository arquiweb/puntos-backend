import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ConceptoUsoPunto } from './concepto-uso-punto.entity';
import { CreateConceptoUsoPuntoDto } from './create-concepto-uso-punto.dto';

@Injectable()
export class ConceptoUsoPuntoService {
  constructor(
    @InjectRepository(ConceptoUsoPunto)
    private conceptoUsoPuntoRepository: Repository<ConceptoUsoPunto>,
  ) {}

  create(createConceptoUsoPuntoDto: CreateConceptoUsoPuntoDto): Promise<ConceptoUsoPunto> {
    const concepto = this.conceptoUsoPuntoRepository.create(createConceptoUsoPuntoDto);
    return this.conceptoUsoPuntoRepository.save(concepto);
  }

  findAll(): Promise<ConceptoUsoPunto[]> {
    return this.conceptoUsoPuntoRepository.find();
  }

  findOne(id: number): Promise<ConceptoUsoPunto> {
    return this.conceptoUsoPuntoRepository.findOneBy({ id });
  }

  async update(id: number, updateConceptoUsoPuntoDto: CreateConceptoUsoPuntoDto): Promise<ConceptoUsoPunto> {
    await this.conceptoUsoPuntoRepository.update(id, updateConceptoUsoPuntoDto);
    return this.findOne(id);
  }

  async remove(id: number): Promise<void> {
    await this.conceptoUsoPuntoRepository.delete(id);
  }
}
