import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { PointBag } from '../point-bag/point-bag.entity';

@Entity()
export class Customer {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  nombre: string;

  @Column()
  apellido: string;

  @Column()
  numeroDocumento: string;

  @Column()
  tipoDocumento: string;

  @Column()
  nacionalidad: string;

  @Column()
  email: string;

  @Column()
  telefono: string;

  @Column()
  fechaNacimiento: string;

  @OneToMany(() => PointBag, pointBag => pointBag.customer)
  pointBags: PointBag[];
}
