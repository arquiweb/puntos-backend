import { Controller, Get, Post, Put, Body, Query, Delete, Param } from '@nestjs/common';
import { CustomerService } from './customer.service';
import { Customer } from './customer.entity';
import { CreateCustomerDto } from './create-customer.dto';

@Controller('customer')
export class CustomerController {
  constructor(private readonly customerService: CustomerService) {}

  @Get('search')
  searchCustomers(
    @Query('nombre') nombre?: string,
    @Query('apellido') apellido?: string,
    @Query('fechaNacimiento') fechaNacimiento?: string,
  ): Promise<Customer[]> {
    return this.customerService.search(nombre, apellido, fechaNacimiento);
  }

  @Post()
  createCustomer(@Body() createCustomerDto: CreateCustomerDto): Promise<Customer> {
    return this.customerService.create(createCustomerDto);
  }

  @Delete(':id')
  async deleteCustomer(@Param('id') id: number): Promise<void> {
    return this.customerService.remove(id);
  }

  @Put(':id')
  updateCustomer(@Param('id') id: number, @Body() updateCustomerDto: CreateCustomerDto): Promise<Customer> {
    return this.customerService.update(id, updateCustomerDto);
  }

  @Get(':id')
  async findOne(@Param('id') id: number): Promise<Customer> {
    return this.customerService.findOne(id);
  }

  @Get()
  findAll() {
    return this.customerService.findAll();
  }
}
