import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Customer } from './customer.entity';
import { CreateCustomerDto } from './create-customer.dto';

@Injectable()
export class CustomerService {
  constructor(
    @InjectRepository(Customer)
    private customersRepository: Repository<Customer>,
  ) {}

  async search(nombre?: string, apellido?: string, fechaNacimiento?: string): Promise<Customer[]> {
    const query = this.customersRepository.createQueryBuilder('customer');

    if (nombre) {
      query.andWhere('customer.nombre LIKE :nombre', { nombre: `%${nombre}%` });
    }

    if (apellido) {
      query.andWhere('customer.apellido LIKE :apellido', { apellido: `%${apellido}%` });
    }

    if (fechaNacimiento) {
      query.andWhere('customer.fechaNacimiento = :fechaNacimiento', { fechaNacimiento });
    }

    return query.getMany();
  }

  async create(createCustomerDto: CreateCustomerDto): Promise<Customer> {
    const customer = this.customersRepository.create(createCustomerDto);
    return this.customersRepository.save(customer);
  }

  async remove(id: number): Promise<void> {
    await this.customersRepository.delete(id);
  }

  async findOne(id: number): Promise<Customer> {
    return this.customersRepository.findOneById(id);
  }

  async update(id: number, updateCustomerDto: CreateCustomerDto): Promise<Customer> {
    await this.customersRepository.update(id, updateCustomerDto);
    return this.findOne(id);
  }

  findAll(): Promise<Customer[]> {
    return this.customersRepository.find();
  }
}
