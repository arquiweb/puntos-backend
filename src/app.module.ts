import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CustomerModule } from './customer/customer.module';
import { ConceptoUsoPuntoModule } from './point-concept/concepto-uso-punto.module';
import { ReglaAsignacionPuntoModule } from './point-ruler/regla-asignacion-punto.module';
import { PointExpirationModule } from './point-expiration/point-expiration.module';
import { PointBagModule } from './point-bag/point-bag.module';
import { PointUsageModule } from './point-usage/point-usage.module';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '',
      database: 'puntos',
      autoLoadEntities: true,
      synchronize: true,
    }),
    CustomerModule,
    ConceptoUsoPuntoModule,
    ReglaAsignacionPuntoModule,
    PointExpirationModule,
    PointBagModule,
    PointUsageModule,
    ScheduleModule.forRoot()
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
