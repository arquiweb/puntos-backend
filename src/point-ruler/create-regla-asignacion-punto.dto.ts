import { IsInt, IsNotEmpty, IsOptional } from 'class-validator';

export class CreateReglaAsignacionPuntoDto {
  @IsOptional()
  @IsInt()
  limiteInferior?: number;

  @IsOptional()
  @IsInt()
  limiteSuperior?: number;

  @IsNotEmpty()
  @IsInt()
  montoEquivalencia: number;
}
