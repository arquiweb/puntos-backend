import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ReglaAsignacionPunto } from './regla-asignacion-punto.entity';
import { CreateReglaAsignacionPuntoDto } from './create-regla-asignacion-punto.dto';

@Injectable()
export class ReglaAsignacionPuntoService {
  constructor(
    @InjectRepository(ReglaAsignacionPunto)
    private reglaAsignacionPuntoRepository: Repository<ReglaAsignacionPunto>,
  ) {}

  create(createReglaAsignacionPuntoDto: CreateReglaAsignacionPuntoDto): Promise<ReglaAsignacionPunto> {
    const regla = new ReglaAsignacionPunto();
    regla.limiteInferior = createReglaAsignacionPuntoDto.limiteInferior;
    regla.limiteSuperior = createReglaAsignacionPuntoDto.limiteSuperior;
    regla.montoEquivalencia = createReglaAsignacionPuntoDto.montoEquivalencia;

    return this.reglaAsignacionPuntoRepository.save(regla);
  }

  findAll(): Promise<ReglaAsignacionPunto[]> {
    return this.reglaAsignacionPuntoRepository.find();
  }

  findOne(id: number): Promise<ReglaAsignacionPunto> {
    return this.reglaAsignacionPuntoRepository.findOneBy({ id });
  }

  async update(id: number, updateReglaAsignacionPuntoDto: CreateReglaAsignacionPuntoDto): Promise<ReglaAsignacionPunto> {
    await this.reglaAsignacionPuntoRepository.update(id, updateReglaAsignacionPuntoDto);
    return this.findOne(id);
  }

  async remove(id: number): Promise<void> {
    await this.reglaAsignacionPuntoRepository.delete(id);
  }

  async getReglaByMonto(montoOperacion: number): Promise<ReglaAsignacionPunto> {
    return this.reglaAsignacionPuntoRepository
      .createQueryBuilder('regla')
      .where('regla.limiteInferior <= :montoOperacion', { montoOperacion })
      .andWhere('regla.limiteSuperior >= :montoOperacion', { montoOperacion })
      .getOne();
  }
}
