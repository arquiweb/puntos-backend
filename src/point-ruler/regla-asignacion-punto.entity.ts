import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class ReglaAsignacionPunto {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true })
  limiteInferior: number;

  @Column({ nullable: true })
  limiteSuperior: number;

  @Column()
  montoEquivalencia: number;
}
