import { Controller, Get, Post, Body, Put, Param, Delete } from '@nestjs/common';
import { ReglaAsignacionPuntoService } from './regla-asignacion-punto.service';
import { CreateReglaAsignacionPuntoDto } from './create-regla-asignacion-punto.dto';

@Controller('regla-asignacion-punto')
export class ReglaAsignacionPuntoController {
  constructor(private readonly reglaAsignacionPuntoService: ReglaAsignacionPuntoService) { }

  @Post()
  create(@Body() createReglaAsignacionPuntoDto: CreateReglaAsignacionPuntoDto) {
    return this.reglaAsignacionPuntoService.create(createReglaAsignacionPuntoDto);
  }

  @Get()
  findAll() {
    return this.reglaAsignacionPuntoService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: number) {
    return this.reglaAsignacionPuntoService.findOne(id);
  }

  @Get('monto/:monto')
  getReglaByMonto(@Param('monto') monto: number) {
    return this.reglaAsignacionPuntoService.getReglaByMonto(monto);
  }

  @Put(':id')
  update(@Param('id') id: number, @Body() updateReglaAsignacionPuntoDto: CreateReglaAsignacionPuntoDto) {
    return this.reglaAsignacionPuntoService.update(id, updateReglaAsignacionPuntoDto);
  }

  @Delete(':id')
  remove(@Param('id') id: number) {
    return this.reglaAsignacionPuntoService.remove(id);
  }
}
