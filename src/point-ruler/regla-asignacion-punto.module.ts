import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ReglaAsignacionPunto } from './regla-asignacion-punto.entity';
import { ReglaAsignacionPuntoService } from './regla-asignacion-punto.service';
import { ReglaAsignacionPuntoController } from './regla-asignacion-punto.controller';

@Module({
  imports: [TypeOrmModule.forFeature([ReglaAsignacionPunto])],
  providers: [ReglaAsignacionPuntoService],
  controllers: [ReglaAsignacionPuntoController],
  exports: [TypeOrmModule.forFeature([ReglaAsignacionPunto]), ReglaAsignacionPuntoService],
})
export class ReglaAsignacionPuntoModule {}
